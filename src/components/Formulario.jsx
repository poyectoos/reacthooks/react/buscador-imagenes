import { useState } from 'react';
import PropTypes from 'prop-types';

import Error from './Error';

const Formulario = ({ actualizarBusqueda }) => {

  const [ termino, actualizarTermino ] = useState('');
  const [ error, actualizarError ] = useState(false);

  const handleSubmit = e => {
    e.preventDefault();

    if (termino.trim() === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);

    // Enviar el termino al api
    actualizarBusqueda(termino);
  }

  return (
    <form
      onSubmit={ handleSubmit }
    >
      <div className="row">
        <div className="form-group col-md-8">
          <input
            type="text"
            className="form-control form-control-lg"
            value={ termino }
            onChange={ e => actualizarTermino(e.target.value) }
          />
        </div>
        <div className="form-group col-md-4">
          <button
            type="submit"
            className="btn btn-danger btn-block btn-lg"
          >
            Buscar
          </button>
        </div>
        { 
          error
            ?
          <Error
            mensaje="Agrega un termino de busqueda"
          />
            :
          null
        }
      </div>
    </form>
  );
};

Formulario.propTypes = {
  actualizarBusqueda:  PropTypes.func.isRequired
};

export default Formulario;