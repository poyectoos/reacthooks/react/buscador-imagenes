import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ mensaje }) => {
  return (
    <div className="col-md-12">
      <div className="alert alert-primary py-1">
        <p className="my-1"><b>Ups!!!</b> { mensaje }</p>
      </div>
    </div>
  );
};

Error.propTypes = {
  mensaje: PropTypes.string.isRequired
};

export default Error;