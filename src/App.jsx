import { Fragment, useState, useEffect } from 'react';

import Formulario from './components/Formulario';
import Imagenes from './components/Imagenes';

function App() {

  const [ busqueda, actualizarBusqueda ] = useState('');
  const [ imagenes, actualizarImagenes ] = useState([]);
  
  const [ pagina, actualizarPagina ] = useState(1);
  const [ total, actualizarTotal ] = useState(1);

  useEffect(() => {
    if (busqueda !== '') {
      consutar();
    }
    // eslint-disable-next-line
  }, [busqueda, pagina]);
  
  const paginaAnterior = () => {
    if (pagina < 2) return;
    actualizarPagina(pagina-1);
  }
  const paginaSiguiente = () => {
    if (pagina > total-1) return;
    actualizarPagina(pagina+1);
  }

  const consutar = async () => { 
    const tamano = 10;
    const KEY = '19408612-85755b3396956a50aa3905122';
    const URI = `https://pixabay.com/api/?key=${KEY}&q=${busqueda}&lang=es&per_page=${tamano}&page=${pagina}`;
    // 

    const respuesta = await fetch(URI);
    const data = await respuesta.json();

    actualizarImagenes(data.hits);
    actualizarTotal(Math.ceil(data.totalHits/tamano));
    
        // Mover la pantalla hacia arriba
    const jumbotron = document.querySelector('.jumbotron');
    jumbotron.scrollIntoView({ behavior: 'smooth' })
  }

  return (
    <Fragment>
      <div className="jumbotron py-5">
        <div className="container">
          <div className="lead text-center">
            <h3 className="mb-4">Buscador de imagenes</h3>
            <Formulario
              actualizarBusqueda={ actualizarBusqueda }
            />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row justify-content-center ">
          <Imagenes imagenes={ imagenes } />
          {
            pagina > 1
              ?
            <button
              type="button"
              className="btn btn-info mx-1 mb-5"
              onClick={ paginaAnterior }
            >
              &laquo; Anterior
            </button>
              :
            null
          }
          {
            pagina < total
              ?
            <button
              type="button"
              className="btn btn-info mx-1 mb-5"
              onClick={ paginaSiguiente }
            >
              Siguiente &raquo;
            </button>
              :
            null
          }
        </div>
      </div>
    </Fragment>
  );
}

export default App;
